package bo.mc4.edson.pruebas.web;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.mc4.edson.pruebas.model.Categoria;
import bo.mc4.edson.pruebas.model.Producto;
import bo.mc4.edson.pruebas.util.FacesUtil;
import bo.mc4.edson.pruebas.util.SessionUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ManagedBean
@ViewScoped
public class RegistroBean implements Serializable {

	// variables para los atributos del objeto persona
	private String nombre = "hola";
	private String descripcion = "";
	private Date fechav;
	private String tipo = "";
	private String categoria = "";
	private boolean fragil = false;
	private String descripcioncliente = "";
	// variable de objeto producto
	private Producto producto;
	// variable de lista de objeto categoria
	private List<Categoria> listaCategoria;
	private String idCategoria;
	

	@PostConstruct
	public void init() {
		// obteniendo el producto seleccionado
		producto = (Producto) SessionUtil.getSession("claveProducto");
		// cargando datos a categoria
		cargarCategoria();
		// mostrando los datos del producto obtenido
		System.out.println("Mostrando producto: " + producto);
		// verificando si el producto obtenido es null
		if (producto == null) {
			producto = new Producto();

		}
	}

	// Metodo para cargar Categorias
	public void cargarCategoria() {
		this.listaCategoria = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			Categoria categoriaO = new Categoria();
			categoriaO.setCodCategoria("COD"+i);
			categoriaO.setNombreCategoria("Categoria" + i);
			this.listaCategoria.add(categoriaO);
		}
	}

	// Metodo para volver a lapagina de registro
	public void volver() {
		FacesUtil.redireccionar("http://localhost:8080/sias/listado.xhtml");
	}

	// Metodo para guardar datos de la clase producto
	public void guardar() {
		try {
			System.out.println("idCategoria: "+idCategoria);
			if(idCategoria==null) {
				throw new IllegalArgumentException("Debe completar la categoria");
			}
			if(idCategoria.equals(new Long("0"))) {
				throw new IllegalArgumentException("Debe completar la categoria");
			}
			FacesUtil.redireccionar("http://localhost:8080/sias/listado.xhtml");
		}
		catch (IllegalArgumentException e) {
			FacesUtil.error(e.getMessage());
		}
	}

	// metodo para mostrar datos del objeto producto
	public void mostrar() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		System.out.println("nombre: " + this.nombre);
		System.out.println("descripcion: " + this.descripcion);
		System.out.println("fecha vencimiento: " + sdf.format(this.fechav));
		System.out.println("tipo: " + this.tipo);
		System.out.println("fragil: " + this.fragil);
		System.out.println("categoria: " + this.categoria);
		System.out.println("descripcion cliente: " + this.descripcioncliente);
	}

}
