package bo.mc4.edson.pruebas.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.mc4.edson.pruebas.model.Categoria;
import bo.mc4.edson.pruebas.model.Producto;
import bo.mc4.edson.pruebas.util.FacesUtil;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class FormulariosBean implements Serializable {

    // variables para los atributos del objeto persona
    private String nombre = "";
    private String descripcion = "";
    private Date fechav;
    private String tipo = "";
    private boolean fragil = false;
    private String descripcioncliente = "";
    // variable de objeto producto
    private Producto producto;
    // variable de lista de objeto categoria
    private List<Categoria> listaCategoria;
    private int size = 5;
    private boolean visible;
    private boolean visible1 = true;
    private String idCategoria;


    @PostConstruct
    public void init() {
        cargarCategoria();
        producto = new Producto();
    }

    // Metodo para cargar Categorias
    public void cargarCategoria() {
        this.listaCategoria = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Categoria categoriaO = new Categoria();
            categoriaO.setCodCategoria("COD" + i);
            categoriaO.setNombreCategoria("Categoria" + i);
            this.listaCategoria.add(categoriaO);
        }
    }

    public void guardar() {
        try {
            System.out.println("idCategoria: " + idCategoria);
            if (idCategoria == null) {
                throw new IllegalArgumentException("Debe completar la categoria");
            }
            if (idCategoria.equals(new Long("0"))) {
                throw new IllegalArgumentException("Debe completar la categoria");
            }
            visible = false;
            visible1 = true;
        } catch (IllegalArgumentException e) {
            FacesUtil.error(e.getMessage());
        }
    }

    public void mostrar() {
        System.out.println("prueba");
    }

    public void show() {
        System.out.println("mostrando el formuario");
        visible = true;
        visible1 = false;
    }

    public void hiden() {
        visible = false;
        visible1 = true;
    }

}
