package bo.mc4.edson.pruebas.web;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.mc4.edson.pruebas.util.FacesUtil;
import bo.mc4.edson.pruebas.util.SessionUtil;
import bo.mc4.edson.pruebas.util.StringUtil;

/**
 * Bean para la clase index.xhtml
 * @author Edson Quinajo
 *
 */
@ManagedBean
@ViewScoped
public class LoginBean {

	//Variable por defecto de la pagina
	private String value = "";
	//Variables para la utenticacion del usuario
	private String usuario = "";
	private String contrasenia = "";

	/**
	 * Metodo que realiza la autenticacion del usuario
	 */
	public void mostrar() {
		try {
			//Iniciando validaciondes de usuario
			if(StringUtil.isEmptyOrNull(usuario)) {
				throw new IllegalArgumentException("Completar el campo Usuario");
			}
			if(StringUtil.isEmptyOrNull(contrasenia)) {
				throw new IllegalArgumentException("Completar el campo Contrasenia");
			}
			if(!usuario.equals("admin")) {
				throw new IllegalArgumentException("Usuario no registrado");
			}
			if(!contrasenia.equals("admin")) {
				throw new IllegalArgumentException("Contrasenia incorecta");
			}
			SessionUtil.iniciarSession(usuario);
			FacesUtil.redireccionar("http://localhost:8080/sias/listado.xhtml");
		}
		catch (IllegalArgumentException e) {
			FacesUtil.error(e.getMessage());
		}
		catch (Exception e) {
			FacesUtil.defauldError();
			e.printStackTrace();
		}
		
	}

	//Get de usuario
	public String getUsuario() {
		return usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}