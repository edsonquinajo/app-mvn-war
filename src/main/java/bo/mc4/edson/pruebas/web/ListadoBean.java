package bo.mc4.edson.pruebas.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.datatable.feature.SelectionFeature;
import org.primefaces.context.RequestContext;

import bo.mc4.edson.pruebas.model.Categoria;
import bo.mc4.edson.pruebas.model.Producto;
import bo.mc4.edson.pruebas.util.DateUtil;
import bo.mc4.edson.pruebas.util.FacesUtil;
import bo.mc4.edson.pruebas.util.LongUtil;
import bo.mc4.edson.pruebas.util.SessionUtil;
import bo.mc4.edson.pruebas.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class ListadoBean implements Serializable {

    private String usuario;
    // variable de lista de productos
    private List<Producto> listaProductos;
    private Producto productoSeleccionado;
    private List<Producto> listaProductosFilter;
    // Variables para la edicion y creacion del producto
    private List<Categoria> listaCategorias;
    private String codCategoria;
    // variable para el autocompletado
    private String txt1;

    @PostConstruct
    public void init() {
        usuario = (String) SessionUtil.getSession("username");
        cargar();
    }

    // metodo para cargar productos a una lista de productos
    public void cargar() {
        listaProductos = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Producto producto = new Producto();
            producto.setIdProducto(new Long(i));
            producto.setNombre("Producto " + i);
            producto.setDescripcion("Descripcion " + i);
            if (i % 2 == 0) {
                producto.setTipo("Activo");
                producto.setFragil(true);
            } else {
                producto.setTipo("Pasivo");
                producto.setFragil(false);
            }
            Date fecha = new Date();
            producto.setFechaVencimiento(fecha);
            producto.setCategoria("Categoria " + i);
            producto.setDescripcionCliente("Descripcion cliente " + i);
            listaProductos.add(producto);

        }
    }

    // funcion para el autocompletado
    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            results.add(query + i);
        }

        return results;
    }

    // Metodo para direccionar a la pagina de registro para un nuevo registro
    public void registroProducto() {
        SessionUtil.setSession("claveProducto", null);
        FacesUtil.redireccionar("http://localhost:8080/sias/registro.xhtml");
    }

    // Metodo para selecionar el producto para la modificacion
    public void selectProducto() {
        SessionUtil.setSession("claveProducto", productoSeleccionado);
        FacesUtil.redireccionar("http://localhost:8080/sias/registro.xhtml");
    }

    // Metodo para registra nuevo producto o editar
    public void nuevoProducto() {
        productoSeleccionado = new Producto();
        cargarCategorias();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg1').show();");
    }

    public void modificarProducto() {
        cargarCategorias();
        codCategoria = productoSeleccionado.getCategoria();
        System.out.println(productoSeleccionado);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg1').show();");
    }

    // Metodo para cargar categoria
    public void cargarCategorias() {
        codCategoria = null;
        listaCategorias = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Categoria categoria = new Categoria();
            categoria.setCodCategoria("COD" + i);
            categoria.setNombreCategoria("Categoria " + i);
            listaCategorias.add(categoria);
        }
    }

    public void guardar() {
        try {
            System.out.println(productoSeleccionado);

            if (StringUtil.isEmptyOrNull(productoSeleccionado.getNombre())) {
                throw new IllegalArgumentException("Debe completar el nombre de la categoria");
            }
            if (StringUtil.isEmptyOrNull(productoSeleccionado.getDescripcion())) {
                throw new IllegalArgumentException("Debe completar la descripcion de la categoria");
            }
            if (productoSeleccionado.getFechaVencimiento() == null) {
                throw new IllegalArgumentException("Debe completar la fecha de vencimiento de la categoria");
            }
            if (StringUtil.isEmptyOrNull(productoSeleccionado.getTipo())) {
                throw new IllegalArgumentException("Debe completar el tipo de categoria");
            }
            if (StringUtil.isEmptyOrNull(codCategoria)) {
                throw new IllegalArgumentException("Debe completar la categoria");
            }
            if (StringUtil.isEmptyOrNull(productoSeleccionado.getDescripcionCliente())) {
                throw new IllegalArgumentException("Debe completar la descripcion del cliente");
            }
            productoSeleccionado.setCategoria(codCategoria);
            if (LongUtil.isNullOrZero(productoSeleccionado.getIdProducto())) {
                productoSeleccionado.setIdProducto(new Long(DateUtil.generateKey()));
            }
            System.out.println("Adicionano al listado");
            if (!verificaProductoLista(productoSeleccionado.getIdProducto())) {
                listaProductos.add(productoSeleccionado);
            }
            System.out.println("Cerrando el dialogo");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dlg1').hide();");
        } catch (IllegalArgumentException e) {
            FacesUtil.error(e.getMessage());
        }
    }

    // Metodo para verficar si el producto existe en la lista
    public boolean verificaProductoLista(Long idProducto) {
        for (Producto producto : listaProductos) {
            if (producto.getIdProducto().equals(idProducto)) {
                System.out.println("siii");
                return true;
            }
        }
        return false;
    }

    // Metodo para direccionar a la pagina index
    public void salir() {
        FacesUtil.redireccionar("http://localhost:8080/sias/login.xhtml");
    }

}