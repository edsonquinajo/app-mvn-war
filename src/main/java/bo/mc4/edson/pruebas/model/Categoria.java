package bo.mc4.edson.pruebas.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Categoria {

	private String codCategoria;
	private String nombreCategoria;
	
	@Override
	public String toString() {
		return "Categoria [codCategoria=" + codCategoria + ", nombreCategoria=" + nombreCategoria + "]";
	}
	
	
}
