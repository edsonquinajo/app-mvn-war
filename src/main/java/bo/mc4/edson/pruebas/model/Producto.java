package bo.mc4.edson.pruebas.model;

import java.util.Date;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Producto {

	private Long idProducto;
	private String nombre;
	private String descripcion;
	private Date fechaVencimiento;
	private String tipo;
	private String categoria;
	private boolean fragil;
	private String descripcionCliente;

	@Override
	public String toString() {
		return "Producto [idProducto=" + idProducto + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", fechaVencimiento=" + fechaVencimiento + ", tipo=" + tipo +", categoria="+categoria+", fragil=" + fragil
				+ ", descripcionCliente=" + descripcionCliente + "]";
	}

}
