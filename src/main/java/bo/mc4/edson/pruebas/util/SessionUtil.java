package bo.mc4.edson.pruebas.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import bo.mc4.edson.pruebas.model.Producto;

public class SessionUtil {
	
	public static void iniciarSession(String username) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("username", username);
	}
	
	public static Object getSession(String key) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		return session.getAttribute(key);
	}
	
	public static void setSession(String llave,Object valor) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		session.setAttribute(llave,valor);
	}

}
