package bo.mc4.edson.pruebas.util;

public class StringUtil {
	
	public static boolean isEmptyOrNull(String valor) {
		if(valor==null) {
			return true;
		}
		if(valor.trim().equals("")) {
			return true;
		}
		return false;
	}
}
