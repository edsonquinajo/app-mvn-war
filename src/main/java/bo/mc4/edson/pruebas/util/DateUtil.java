package bo.mc4.edson.pruebas.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static String generateKey() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return sdf.format(new Date());
	}
}
