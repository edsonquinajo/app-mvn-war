package bo.mc4.edson.pruebas.util;

public class LongUtil {
	
	public static boolean isNullOrZero(Long valor) {
		if(valor==null) {
			return true;
		}
		if(valor.equals(new Long("0"))) {
			return true;
		}
		return false;
	}

}
